global
    log /dev/log len 4096 local0
    log /dev/log len 4096 local1 notice
    chroot /var/lib/haproxy
    stats socket /run/haproxy/admin.sock mode 660 level admin
    stats socket ipv4@10.0.0.2:23646 level admin
    stats timeout 30s
    user haproxy
    group haproxy
    daemon
    maxconn 20000 # raising in the defaults without raising global makes no sense (!!)
    nbproc 1
    nbthread 1
    hard-stop-after 5m
    server-state-file /etc/haproxy/state/global

    # Default SSL material locations
    ca-base /etc/ssl/certs
    crt-base /etc/ssl/private

    # Default ciphers to use on SSL-enabled listening sockets.
    # For more information, see ciphers(1SSL).
    ssl-default-bind-ciphers ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4
    # Keep DH params size at 1024 for Java 6 HTTPS clients
    tune.ssl.default-dh-param 1024

defaults
    log global
    option dontlognull
    # Use all backup servers
    # instead of just the first one
    # if other servers are down
    option allbackups
    maxconn 20000 # raising in the defaults without raising global makes no sense (!!)
    timeout connect 5000
    timeout check 30000
    timeout client 90s
    timeout server 1h
    errorfile 400 /etc/haproxy/errors/400.http
    errorfile 403 /etc/haproxy/errors/429.http
    errorfile 408 /etc/haproxy/errors/400.http
    errorfile 429 /etc/haproxy/errors/429.http
    errorfile 500 /etc/haproxy/errors/500.http
    errorfile 502 /etc/haproxy/errors/502.http
    errorfile 503 /etc/haproxy/errors/503.http
    errorfile 504 /etc/haproxy/errors/504.http
    load-server-state-from-file global

listen stats
    bind 0.0.0.0:7331
    mode http
    stats enable
    stats hide-version
    stats realm Haproxy\ Statistics
    stats uri /
    stats auth admin:this-is-a-test-password
    stats admin if TRUE

frontend altssh
    bind 0.0.0.0:443
    mode tcp
    option splice-auto

    acl deny-403-ip src -f /etc/haproxy/front-end-security/deny-403-ips.lst
    tcp-request content reject if deny-403-ip

    timeout client-fin 5s
    log-format %ci:%cp\ [%t]\ %ft\ %sslv\ %b/%s\ %Tw/%Tc/%Tt\ %B\ %U\ %ts\ %ac/%fc/%bc/%sc/%rc\ %sq/%bq



    default_backend altssh

frontend check_ssh
    bind 0.0.0.0:8003
    mode http
    option splice-auto
    acl no_be_srvs_ssh nbsrv(altssh) lt 1
    monitor-uri /-/available-ssh
    monitor fail if no_be_srvs_ssh

backend altssh
    mode tcp
    balance roundrobin
    option splice-auto
    timeout server-fin 5s
    timeout server 2h
    server altssh0.local 127.0.0.1:22 weight 100 check port 22 inter 5000 fall 3 verify none
    server cny-altssh0.local 127.0.0.1:22 weight 0 check port 22 inter 5000 fall 3 verify none
