global
    log /dev/log len 4096 local0
    log /dev/log len 4096 local1 notice
    chroot /var/lib/haproxy
    stats socket /run/haproxy/admin.sock mode 660 level admin
    stats socket ipv4@10.0.0.2:23646 level admin
    stats timeout 30s
    user haproxy
    group haproxy
    daemon
    maxconn 20000 # raising in the defaults without raising global makes no sense (!!)
    nbproc 1
    nbthread 1
    hard-stop-after 5m
    server-state-file /etc/haproxy/state/global

    # Default SSL material locations
    ca-base /etc/ssl/certs
    crt-base /etc/ssl/private

    # Default ciphers to use on SSL-enabled listening sockets.
    # For more information, see ciphers(1SSL).
    ssl-default-bind-ciphers ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4
    # Keep DH params size at 1024 for Java 6 HTTPS clients
    tune.ssl.default-dh-param 1024

defaults
    log global
    option dontlognull
    # Use all backup servers
    # instead of just the first one
    # if other servers are down
    option allbackups
    maxconn 20000 # raising in the defaults without raising global makes no sense (!!)
    timeout connect 5000
    timeout check 30000
    timeout client 90s
    timeout server 1h
    errorfile 400 /etc/haproxy/errors/400.http
    errorfile 403 /etc/haproxy/errors/429.http
    errorfile 408 /etc/haproxy/errors/400.http
    errorfile 500 /etc/haproxy/errors/500.http
    errorfile 502 /etc/haproxy/errors/502.http
    errorfile 503 /etc/haproxy/errors/503.http
    errorfile 504 /etc/haproxy/errors/504.http
    load-server-state-from-file global

listen stats
    bind 0.0.0.0:7331
    mode http
    stats enable
    stats hide-version
    stats realm Haproxy\ Statistics
    stats uri /
    stats auth admin:this-is-a-test-password
    stats admin if TRUE

#---------------------------------------------------------------------
# Sync data between the pages nodes
#---------------------------------------------------------------------
peers pages-peers
    peer fe-pages01.sv.gitlab.com 10.65.1.101:32768

frontend pages_http
    bind 0.0.0.0:80
    mode http
    option splice-auto

    acl deny-403-ip src -f /etc/haproxy/front-end-security/deny-403-ips.lst
    http-request deny if deny-403-ip

    capture request header Host len 40
    log-format %ci:%cp\ [%t]\ %ft\ %sslv\ %b/%s\ %Tq/%Tw/%Tc/%Tr/%Tt\ %ST\ %B\ %U\ %CC\ %CS\ %tsc\ %ac/%fc/%bc/%sc/%rc\ %sq/%bq\ %hr\ %hs\ %{+Q}r
    acl doc_gitlab_com hdr_beg(host) -i doc.gitlab.com
    http-request redirect location https://docs.gitlab.com%[path] if doc_gitlab_com
    use_backend pages_http
    http-request deny deny_status 400 if is_download

frontend pages_https
    bind 0.0.0.0:443
    mode tcp
    option splice-auto

    tcp-request inspect-delay 5s
    acl deny-403-ip src -f /etc/haproxy/front-end-security/deny-403-ips.lst
    use_backend deny_https if deny-403-ip

    log-format %ci:%cp\ [%t]\ %ft\ %sslv\ %b/%s\ %Tw/%Tc/%Tt\ %B\ %U\ %ts\ %ac/%fc/%bc/%sc/%rc\ %sq/%bq
    use_backend pages_https
    http-request deny deny_status 500 if is_download

frontend check_http
    bind 0.0.0.0:8001
    mode http
    option splice-auto
    acl no_be_srvs_http nbsrv(pages_http) lt 1
    monitor-uri /-/available-http
    monitor fail if no_be_srvs_http

frontend check_https
    bind 0.0.0.0:8002
    mode http
    option splice-auto
    acl no_be_srvs_https nbsrv(pages_https) lt 1
    monitor-uri /-/available-https
    monitor fail if no_be_srvs_https

backend pages_http
    mode http
    balance roundrobin
    option forwardfor
    option splice-auto
    option httpchk GET /-/readiness HTTP/1.1\r\nHost:\ gitlab.com
    server pages-01.stg.gitlab.com 127.0.0.1:1080 weight 100 check inter 3s fastinter 1s downinter 5s fall 3

backend pages_https
    mode tcp
    option splice-auto
    balance roundrobin
    option httpchk GET /-/readiness HTTP/1.1\r\nHost:\ gitlab.com
    server pages-01.stg.gitlab.com 127.0.0.1:1443 weight 100 check inter 3s fastinter 1s downinter 5s fall 3 port 1080

backend deny_https
    mode tcp
    tcp-request content reject

backend deny_http
    mode http
    http-request deny
