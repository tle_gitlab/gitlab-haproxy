frontend https_git_ci_gateway
    bind 0.0.0.0:8989 ssl crt /etc/haproxy/ssl/ci-gateway.pem no-sslv3
    mode http
    option splice-auto
    log-format %ci:%cp\ [%t]\ %ft\ %sslv\ %b/%s\ %Tq/%Tw/%Tc/%Tr/%Tt\ %ST\ %B\ %U\ %CC\ %CS\ %tsc\ %ac/%fc/%bc/%sc/%rc\ %sq/%bq\ %hr\ %hs\ %{+Q}r
    capture request header User-Agent len 64

    acl is_runner_manager src 10.1.5.0/24
    acl is_api_jobs_request path_reg -i ^\/api\/v\d\/jobs\/request$
    acl is_api_jobs_id path_reg -i ^\/api\/v\d\/jobs\/\d+$
    acl is_api_jobs_id_trace path_reg -i ^\/api\/v\d\/jobs\/\d+\/trace$
    acl is_patch_method method PATCH

    acl is_https_git path_reg -i ^\/(?!help)[^\/]+\/((?!artifacts|browse|file|tree|blob|blame|commits|raw|find_file|files|graphs|logs_tree|refs|wikis|new|create|edit|update|preview|badges)[^\/]+\/)*[^\/]+\.git($|\/)
    acl is_git_method method GET POST

    acl is_git_legacy_initial_request path_reg -i ^\/[^\/]+(\/[^\/]+)+\/info\/refs$
    acl is_git_new_initial_request path_reg -i ^\/[^\/]+(\/[^\/]+)+.git\/info\/refs$

    # Prevent users from setting bypass header
    http-request set-header X-GitLab-RateLimit-Bypass 0

    # Set the canonical host as Host header so that ci-gateway
    # is transparent for GitLab backends
    http-request set-header Host gitlab.com

    ###############################################################
    # Redirect git requests missing .git suffix in the repo url
    use_backend ci_gateway_git_repo_redirection if is_git_legacy_initial_request !is_git_new_initial_request is_git_method
    ###############################################################

    ###############################################################
    # HTTPS git routing
    http-request set-header X-GitLab-RateLimit-Bypass 1 if is_https_git is_git_method
    use_backend https_git if is_https_git is_git_method
    ###############################################################

    ###############################################################
    # Limited API routing
    use_backend api if is_runner_manager is_api_jobs_request METH_POST
    use_backend api if is_runner_manager is_api_jobs_id METH_PUT
    use_backend api if is_runner_manager is_api_jobs_id_trace is_patch_method
    ###############################################################

    ###############################################################
    # Redirect to GitLab public entrypoint otherwise
    default_backend ci_gateway_catch_all
    ###############################################################
