name             'gitlab-haproxy'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license          'All rights reserved'
description      'Manage HAProxy server'
version          '4.15.2'
chef_version     '>= 14.0'

depends 'gitlab-exporters'
depends 'gitlab-mtail'
depends 'gitlab-vault'
depends 'gitlab_secrets'
depends 'apt'
